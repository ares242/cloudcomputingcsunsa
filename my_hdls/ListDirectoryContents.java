package org.jvg;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
//import java.io.FileNotFoundException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

public class ListDirectoryContents {
  public static void main(String[] args) throws IOException, URISyntaxException
  {	
    if(args.length<1){System.out.println("No path given"); return;}
    URI p = new URI(args[0]);
    //Get the URI to the host from the given path
    URI fileSystem = new URI(p.getScheme(),
        p.getUserInfo(), p.getHost(), p.getPort(),
        null, null,null);
    //Get the Configuration instance
    Configuration configuration = new Configuration();
    //Get the instance of the HDFS
    FileSystem hdfs = FileSystem.get(fileSystem, configuration);
    //Calling recursive list
    listFiles(new Path(p),hdfs);
  }

  public static void listFiles(Path dir, FileSystem hdfs) throws IOException, URISyntaxException {
    // Get the metadata of the desired directory
    FileStatus[] fileStatus = hdfs.listStatus(dir);
    //Iterating the files in the directory
    for( FileStatus stat : fileStatus)
    {
      System.out.println(stat.getPath());
      if(stat.isDirectory())
      {
	listFiles(stat.getPath(),hdfs);
      }
    }
  }
}
